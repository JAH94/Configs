TF2 Configuration Files
===========
*My Team Fortress 2 configuration files.*

**Note:** these scripts do not use the `wait` command.

## Settings ##
The config comes with several user editable settings; edit them in `user_configuration\cfg\user\settings.cfg`:
- `AUTO_CROUCH_JUMP`
- `CLEAR_CONSOLE_ON_START`
- `CONSOLE_FILTER`
- `DEBUG_CC_OUTPUT_ON_START`
- `DEBUG_CONSOLE_OUTPUT`
- `DEV_INFO`
- `DOWNLOAD_FILES`
- `FUNCTION_KEYS_CLASS_SWITCHER`
- `GRAPHICS_CONFIG_SWITCHER`
- `KEYPAD_DISGUISE`
- `LOADOUT_SWITCH`
- `MUSIC_PLAYER_CLASSES`
- `NULL_CANCELLING`
- `OPTIMISE_OPENGL`
- `PACKET_RATE`
- `PACKET_SIZE`
- `SHOW_NETGRAPH`
- `SNAPSHOTS`
- `SUICIDE_EXPLOSION`
- `VIEWMODEL_TOGGLING`
- `WAIT_TESTER`
- `ZOOM_IN_HIDE_WEAPON`
- `ZOOM_IN_SENSITIVITY`
- `ZOOM_OUT_SENSITIVITY`

For class specific config settings you can find them in the folder `user_configuration\cfg\class`:
- `CLASS_SPECIFIC_BINDS`
- `CLASS_VIEWMODEL`
- `DISGUISED_WEAPON_SWITCHING`
- `ENGINEER_PDA`
- `EUREKA_EFFECT`
- `KEYPAD_DISGUISE`
- `MOVE_SPAWN_LOCATION`
- `SOLDIER_ROCKET_JUMP`
- `SPY_AUTO_DISGUISE`
- `SPY_TAUNT`

## To Install ##

Extract the Zip and Copy and Paste `Configs` into `tf\custom`, found in the `Team Fortress 2` folder.

## If you need help finding the `Team Fortress 2` folder ##

In `Steam` go to `Library` and find `Team Fortress 2`, right-click it and select `Manage` > `Browse local files`.
This will open up your `Team Fortress 2` folder.

## Known Bugs ##

## Other Files ##
* [Disable Incoming Message](https://drive.google.com/file/d/12EYvAGVP4W4OX7dves0kpylp-4v2ioCB/view)
    * `disable_incoming_message.vpk`
        * Removes Miss Pauling's voice lines for quests.
        * Removes the incoming message hud panel.

* [FlawHUD](https://huds.tf/site/s-FlawHUD) a minimalistic designed HUD with dark themed colours.

* [No Hats Mod](https://github.com/Fedora31/no-hats-bgum/tree/master) removes cosmetics and other things, such as: 
    * `no_botkiller.vpk` Removes Botkiller Weapons.
    * `no_bugged_misc.vpk` Removes bugged Miscs.
    * `no_christmas_light.vpk` Removes Christmas Festiviziers *NOT* Festive Weapons.
    * `no_hats_bgum.vpk` Removes Hats.
    * `no_skins.vpk` Removes Skins.
    * `no_unusuals.vpk` Removes Unsuals.
    * `no_zombie_skins.vpk` Removes Zombie Skins.

## Credits ##
- Config files based on [Lyrositor's TF2 Scripts](https://github.com/Lyrositor/TF2-Scripts/).
- [Team Fortress 2 Wiki](http://wiki.teamfortress.com) for their scripting tutorials.